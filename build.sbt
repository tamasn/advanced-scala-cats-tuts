import scalariform.formatter.preferences._
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats" % "0.9.0",
  "org.specs2" %% "specs2-core" % "3.9.1" % "test")

scalacOptions in Test ++= Seq("-Yrangepos")

wartremoverWarnings ++= Warts.unsafe

val preferences =
  ScalariformKeys.preferences := ScalariformKeys.preferences.value

SbtScalariform.scalariformSettings ++ Seq(preferences)