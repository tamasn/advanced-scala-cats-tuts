import cats.data.EitherT

import scala.concurrent.Future
import cats.instances.future._
import cats.syntax.applicative._
import scala.concurrent.ExecutionContext.Implicits.global

package object transformers {
  type Response[A] = EitherT[Future, String, A]

  val powerLevels = Map(
    "Jazz" -> 6,
    "Bumblebee" -> 8,
    "Hot Rod" -> 10)

  def getPowerLevel(autobot: String): Response[Int] = powerLevels.get(autobot) match {
    case Some(level) => EitherT.pure[Future, String, Int](level)
    case None => EitherT.left[Future, String, Int](s"Unable to reach $autobot".pure[Future])
  }

  def canSpecialMove(ally1: String, ally2: String): Response[Boolean] = for {
    l1 <- getPowerLevel(ally1)
    l2 <- getPowerLevel(ally2)
  } yield l1 + l2 > 15

  def tacticalReport(ally1: String, ally2: String): Response[String] = for {
    can <- canSpecialMove(ally1, ally2)
  } yield s"$ally1 & $ally2 can" + (if (can) "" else "not") + " perform a special move"
}
