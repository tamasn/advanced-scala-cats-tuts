import cats.data.Writer
import cats.syntax.writer._
import cats.syntax.applicative._
import cats.instances.vector._

package object monad {
  trait Monad[F[_]] {
    def pure[A](a: A): F[A]
    def flatMap[A, B](value: F[A])(f: A => F[B]): F[B]

    def map[A, B](value: F[A])(f: A => B): F[B] = flatMap(value)(a => pure(f(a)))
  }

  type Id[A] = A

  implicit val idMonad = new Monad[Id] {
    override def pure[A](value: A) = value
    override def flatMap[A, B](value: Id[A])(f: A => Id[B]) = f(value)
    override def map[A, B](value: A)(f: A => B) = f(value)
  }

  def slowly[A](body: => A) = try body finally Thread.sleep(100)

  type Factorial[A] = Writer[Vector[String], A]

  private def factorialPrime(n: Int): Factorial[Int] = for {
    ans <- slowly(if (n == 0) 1.pure[Factorial] else factorialPrime(n - 1).map(n * _))
    _ <- Vector(s"fact $n $ans").tell
  } yield ans

  def factorial(n: Int): Int = {
    val (log, retVal) = factorialPrime(n).run
    log.foreach(x => println(x))
    retVal
  }
}

/*
import monad._
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

Await.result(Future.sequence(Vector(Future(factorial(3)), Future(factorial(4)))), 5.seconds)

 */ 