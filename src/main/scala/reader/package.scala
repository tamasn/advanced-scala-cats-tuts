import cats.data.Reader

package object reader {
  type DbReader[A] = Reader[Db, A]

  def findUserName(userId: Int): DbReader[Option[String]] =
    Reader(db => db.usernames.get(userId))

  def checkPassword(username: String, password: String): DbReader[Boolean] =
    Reader(_.passwords.get(username).filter(_ == password).isDefined)

  def checkLogin(userId: Int, password: String): DbReader[Boolean] = {
    findUserName(userId).flatMap {
      case None => Reader(_ => false)
      case Some(userName) => checkPassword(userName, password)
    }
  }
}
