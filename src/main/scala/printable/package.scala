import cats.Show
import cats.Eq
import cats.instances.int._
import cats.instances.string._
import cats.syntax.eq._
package object printable {
  trait Printable[A] {
    def format(a: A): String
  }

  object Printable {
    def apply[A](implicit printable: Printable[A]): Printable[A] = printable

    def instance[A](f: A => String): Printable[A] = new Printable[A] {
      override def format(a: A): String = f(a)
    }

    def format[A](a: A)(implicit printable: Printable[A]) = printable.format(a)
    def print[A](a: A)(implicit printable: Printable[A]) = println(printable.format(a))
  }

  object PrintableInstances {
    implicit val stringPrintable = Printable.instance((s: String) => s)
    implicit val intPrintable = Printable.instance((i: Int) => i.toString)
  }

  implicit class PrintableOps[A](value: A)(implicit printable: Printable[A]) {
    def print(): Unit = Printable.print(value)
    def format(): String = Printable.format(value)
  }

  final case class Cat(name: String, age: Int, color: String)

  object Cat {
    implicit val printableCat =
      Printable.instance((c: Cat) => s"${c.name} is a ${c.age} year-old ${c.color} cat")

    implicit val catShow: Show[Cat] =
      Show.show(cat => s"${cat.name} is a ${cat.age} year-old ${cat.color} cat")

    implicit val catEq: Eq[Cat] = Eq.instance((lh: Cat, rh: Cat) =>
      lh.name === rh.name && lh.age === rh.age && lh.color === rh.color)
  }
}
