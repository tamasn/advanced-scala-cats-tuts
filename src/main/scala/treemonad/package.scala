import cats.Monad

package object treemonad {
  sealed trait Tree[+A]

  final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
  final case class Leaf[A](value: A) extends Tree[A]

  def branch[A](left: Tree[A], right: Tree[A]): Tree[A] = Branch(left, right)

  def leaf[A](value: A): Tree[A] = Leaf(value)

  implicit val treeMonad = new Monad[Tree] {
    override def flatMap[A, B](fa: Tree[A])(f: (A) => Tree[B]): Tree[B] = fa match {
      case Leaf(x) => f(x)
      case Branch(l, r) => Branch(flatMap(l)(f), flatMap(r)(f))
    }

    private def go[A, B](a: Tree[Either[A, B]], f: (A) => Tree[Either[A, B]]): Tree[B] = a match {
      case Branch(l, r) => Branch(go(l, f), go(r, f))
      case Leaf(Left(a)) => go(f(a), f)
      case Leaf(Right(b)) => Leaf(b)
    }

    override def tailRecM[A, B](a: A)(f: (A) => Tree[Either[A, B]]): Tree[B] = go(f(a), f)

    override def pure[A](x: A): Tree[A] = leaf(x)
  }
}
