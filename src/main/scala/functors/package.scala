package object functors {
  trait Printable[A] { self =>
    def format(value: A): String

    def contramap[B](f: B => A): Printable[B] = new Printable[B] {
      override def format(b: B): String = self.format(f(b))
    }
  }

  trait Codec[A] { self =>
    def encode(value: A): String
    def decode(value: String): Option[A]

    def imap[B](dec: A => B, enc: B => A): Codec[B] = new Codec[B] {
      override def encode(value: B) = self.encode(enc(value))
      override def decode(value: String) = self.decode(value).map(dec)
    }
  }

  final case class Box[A](value: A)

  implicit def printableBox[A](implicit p: Printable[A]): Printable[Box[A]] =
    p.contramap(b => b.value)

  implicit def boxCodec[A](implicit c: Codec[A]): Codec[Box[A]] =
    c.imap(a => Box(a), b => b.value)
}

