package hadoop

import cats.{ Monad, Monoid, Id }
import cats.syntax.monoid._
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object FoldMap {
  def foldMap[A, B](values: Iterable[A])(func: A => B = (a: A) => a)(implicit M: Monoid[B]): B =
    M.combineAll(values.map(func))

  def foldMapP[A, B: Monoid](values: Iterable[A])(func: A => B = (a: A) => a): Future[B] = {
    val groups = values.size / Runtime.getRuntime.availableProcessors()
    Future.sequence(values.grouped(groups).map { xs =>
      Future(Monoid[B].combineAll(xs.map(func)))
    }).map { xs =>
      Monoid[B].combineAll(xs)
    }
  }

  def foldMapM[A, M[_]: Monad, B: Monoid](iter: Iterable[A])(f: A => M[B] = (a: A) => a.pure[Id]): M[B] = {
    iter.foldLeft(Monad[M].pure(Monoid[B].empty)) {
      case (accM, x) =>
        for {
          acc <- accM
          x2 <- f(x)
        } yield acc |+| x2
    }
  }

  def foldMapViaFoldMapM[A, B](values: Iterable[A])(func: A => B = (a: A) => a)(implicit M: Monoid[B]): B = {
    foldMapM(values)((a: A) => func(a).pure[Id])
  }
}
