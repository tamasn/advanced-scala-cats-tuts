import cats.data.State

package object state {
  type CalcState[A] = State[List[Int], A]

  def operation(op: (Int, Int) => Int): CalcState[Int] =
    for {
      s <- State.get[List[Int]]
      sum = s.drop(1).foldLeft(s.headOption.getOrElse(0))(op)
      _ <- State.modify[List[Int]](_ => List(sum))
    } yield sum

  def evalOne(sym: String): CalcState[Int] = sym match {
    case "+" => operation(_ + _)
    case "-" => operation(_ - _)
    case "*" => operation(_ * _)
    case "/" => operation(_ / _)
    case "^" => operation(_ ^ _)
    case i =>
      val num = i.toInt
      for {
        _ <- State.modify[List[Int]](s => s :+ num)
      } yield num
  }

  def evalAll(input: List[String]): CalcState[Int] = input match {
    case lastItem :: Nil => evalOne(lastItem)
    case item :: rest => for {
      _ <- evalOne(item)
      ans <- evalAll(rest)
    } yield ans
  }
}

