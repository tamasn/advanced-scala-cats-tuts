import cats.Monad
import scala.language.higherKinds
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.cartesian._
import cats.instances.either._
import cats.instances.list._
import cats.data.Validated
import scala.util.Try
package object cartesian {
  def product[M[_]: Monad, A, B](fa: M[A], fb: M[B]): M[(A, B)] = for {
    a <- fa
    b <- fb
  } yield (a, b)

  final case class User(name: String, age: Int)

  def getValue(name: String, fields: Map[String, String]): Either[List[String], String] =
    fields.get(name) match {
      case Some(n) => Right(n)
      case None => Left(List(s"Unable to find $name"))
    }

  def parseInt(field: String): Either[List[String], Int] = Try(field.toInt).toOption match {
    case Some(x) => Right(x)
    case None => Left(List(s"Unable to convert number to int"))
  }

  def nonBlank(field: String): Either[List[String], Unit] =
    if (field.nonEmpty)
      Right(())
    else
      Left(List("Field is empty"))

  def readName(fields: Map[String, String]): Either[List[String], String] =
    for {
      value <- getValue("name", fields)
      _ <- nonBlank(value)
    } yield value

  private def nonNegative(field: Int): Either[List[String], Unit] =
    if (field > 0)
      Right(())
    else
      Left(List(s"Number is less than zero"))

  def readAge(fields: Map[String, String]): Either[List[String], Int] =
    for {
      ageS <- getValue("age", fields)
      age <- parseInt(ageS)
      _ <- nonNegative(age)
    } yield age

  def readUser(fields: Map[String, String]): Validated[List[String], User] = {
    (Validated.fromEither(readName(fields)) |@| Validated.fromEither(readAge(fields))).map(User.apply)
  }

}
