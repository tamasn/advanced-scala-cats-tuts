package object foldable {
  def consFoldLeft[A](l: List[A]): List[A] =
    l.foldLeft(List.empty[A])({ case (acc, i) => i :: acc })

  def consFoldRight[A](l: List[A]): List[A] =
    l.foldRight(List.empty[A])({ case (i, acc) => i :: acc })

  def mapViaFoldRight[A, B](xs: List[A])(f: A => B): List[B] =
    xs.foldRight(List.empty[B])({ case (i, acc) => f(i) :: acc })

  def flatMapViaFoldRight[A, B](xs: List[A])(f: A => List[B]): List[B] =
    xs.foldRight(List.empty[B])({ case (i, acc) => f(i) ::: acc })

  def filterViaFoldRight[A, B](xs: List[A])(f: A => Boolean): List[A] =
    xs.foldRight(List.empty[A]) {
      case (i, acc) if f(i) => i :: acc
      case (_, acc) => acc
    }

  def sumViaFoldRight(xs: List[Int]): Int = xs.foldRight(0)({ case (i, acc) => i + acc })
}
