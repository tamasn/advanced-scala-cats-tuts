package hadoop

import org.specs2.Specification
import cats.instances.all._
import cats.syntax.either._

class FoldMapSpec extends Specification {
  def is = s2"""
    foldMap should work with a string $testWithString
    foldMap should work with another string $testWithAnotherString
    foldMapM should work with option $testFoldMapMWithOption
    foldMapM should work with list $testFoldMapMWithList
    foldMapM should work with filter $testFoldMapMWithFilter
    foldMapM should work with default argument $testFoldMapMDefault
    foldMapViaFoldMapM should work with a string $testFoldMapViaFoldMapMWithString
    foldMapM should short circuit with Either.left $testEither
  """

  def testWithString =
    FoldMap.foldMap(List(1, 2, 3))(_.toString + "! ") === "1! 2! 3! "

  val seq = List(1, 2, 3)

  def testWithAnotherString =
    FoldMap.foldMap("Hello world!")(_.toString.toUpperCase) === "HELLO WORLD!"

  def testFoldMapMWithOption =
    FoldMap.foldMapM(seq)(a => Option(a)) === Some(6)

  def testFoldMapMWithList =
    FoldMap.foldMapM(seq)(x => List(x)) === List(6)

  def testFoldMapMWithFilter =
    FoldMap.foldMapM(seq)(a => if (a % 2 == 0) Option(a) else Option.empty[Int]) === None

  def testFoldMapMDefault =
    FoldMap.foldMapM(seq)() === 6

  def testFoldMapViaFoldMapMWithString =
    FoldMap.foldMapViaFoldMapM(List(1, 2, 3))(_.toString + "! ") === "1! 2! 3! "

  val either1 = Either.catchOnly[NumberFormatException]("Cat".toInt)
  val either2 = Either.catchOnly[NumberFormatException]("1".toInt)
  val eitherSeq = List(either1, either2)

  def testEither =
    FoldMap.foldMapM(eitherSeq)() must beLeft
}
