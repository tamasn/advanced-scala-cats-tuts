package reader

import org.specs2._

class DbReaderSpec extends Specification {
  def is = s2"""
    checkLogin should match to correct password $matching
    checkLogin should fail on not matching password $notMatching
  """

  val db = Db(
    usernames = Map(
      1 -> "dade",
      2 -> "kate",
      3 -> "margo"),
    passwords = Map(
      "dade" -> "zerocool",
      "kate" -> "acidburn",
      "margo" -> "secret"))

  def matching = checkLogin(1, "zerocool").run(db) === true
  def notMatching = checkLogin(4, "davinci").run(db) === false
}
