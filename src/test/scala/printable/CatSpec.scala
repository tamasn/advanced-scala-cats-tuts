package printable

import org.specs2._
import cats.syntax.show._
import cats.Eq

class CatSpec extends Specification {
  def is = s2"""

  Cat should be printable $catPrintable
  Cat should show $catShow
  Eq should work $catEq
  NotEq should work $catNotEq
  """

  val cat = Cat("Tom", 2, "brown")
  val otherCat = Cat("Biff", 3, "red")

  val catFormat = "Tom is a 2 year-old brown cat"

  def catPrintable = cat.format() === catFormat

  def catShow = cat.show === catFormat

  def catEq = Eq.eqv(cat, cat) === true

  def catNotEq = Eq.eqv(cat, otherCat) === false
}
