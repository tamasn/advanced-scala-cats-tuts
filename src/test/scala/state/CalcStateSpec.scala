package state

import org.specs2._

class CalcStateSpec extends Specification {
  def is = s2"""
    parses a single number correctly $singleNumber
    adds up numbers correctly $runSum
    correctly gets the product $runProduct
    correctly gets a more complex calculation $runComplex
  """

  def singleNumber = evalOne("42").runA(Nil).value === 42

  def onePlusTwo = for {
    _ <- evalOne("1")
    _ <- evalOne("2")
    ans <- evalOne("+")
  } yield ans

  def runSum = onePlusTwo.runA(Nil).value === 3

  def runProduct = evalAll(List("2", "4", "*")).runA(Nil).value === 8

  def runComplex = evalAll(List("2", "4", "*", "5", "+", "2", "-")).runA(Nil).value === 11
}
