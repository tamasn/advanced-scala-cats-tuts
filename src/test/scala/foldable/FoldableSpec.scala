package foldable

import org.specs2._
class FoldableSpec extends Specification {
  def is = s2"""

  consFoldLeft builds reverse list $testFoldLeft
  consFoldRight builds identical list $testFoldRight
  """

  def testFoldLeft =
    consFoldLeft(List(1, 2, 3)) === List(3, 2, 1)

  def testFoldRight =
    consFoldRight(List(1, 2, 3)) === List(1, 2, 3)

}
