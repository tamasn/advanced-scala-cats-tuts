import org.specs2.Specification
import org.specs2.concurrent.ExecutionEnv

import scala.concurrent.duration._

class TestAsyncSpec extends Specification {
  def is() = s2"""
    testTotalUptime should work ${implicit ee: ExecutionEnv => testTotalUptime}
  """

  def testTotalUptime(implicit ee: ExecutionEnv) = {
    val hosts = Map("host1" -> 10, "host2" -> 6)
    val client = new TestAsync.TestUptimeClient(hosts)
    val service = new TestAsync.UptimeService(client)
    val actual = service.getTotalUptime(hosts.keys.toList)
    val expected = hosts.values.sum
    actual.map(_ === expected).await
  }
}
