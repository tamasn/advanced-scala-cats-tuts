package treemonad

import org.specs2._
import cats.syntax.functor._
import cats.syntax.flatMap._

class TreeMonadSpec extends Specification {
  def is = s2"""
    map works $mapWorks
    flatMap works $flatMapWorks
  """

  val tree = branch(branch(leaf(5), leaf(2)), leaf(3))
  val doubleTtree = branch(branch(leaf(10), leaf(4)), leaf(6))
  val doubleTreeExpanded = branch(branch(branch(leaf(10), leaf(10)), branch(leaf(4), leaf(4))), branch(leaf(6), leaf(6)))

  def mapWorks = tree.map(_ * 2) === doubleTtree
  def flatMapWorks = (for {
    x <- tree
    dX = x * 2
    y <- branch(leaf(dX), leaf(dX))
  } yield y) === doubleTreeExpanded
}
